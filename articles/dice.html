<p>At the core of most tabletop games is the requirement to roll dice. This page details the concepts and syntax used to execute dice rolls with Foundry VTT's specific roll formula syntax.</p>
<hr />

<h2 id="commands">Roll Modes and Chat Commands</h2>
<p>There are four different roll modes in Foundry Virtual Tabletop. Each roll mode implies a different level of visibility for the resulting roll. Every dice roll becomes a message in the chat log but the visibility of that message and the details of the roll vary for each mode. For every roll mode, all players will be able to see that a roll was performed - just as players at a physical tabletop can hear the audible sound of rolled dice - but depending on the roll mode they may not see the results. The available modes are:</p>
<dl>
	<dt>Normal Roll</dt>
	<dd>
		<p>A "normal roll" is a public dice roll which is visible to all players. To perform a normal roll, use <code>/roll</code> or <code>/r</code> as the command prefix.</p>
	</dd>
	<dt>GM Roll</dt>
	<dd>
		<p>A "GM roll" is a private dice roll where the result is only visible to the player that rolled and any Game Master users. To perform a GM roll, use <code>/gmroll</code> or <code>/gmr</code> as the command prefix.</p>
	</dd>
	<dt>Blind Roll</dt>
	<dd>
		<p>A "blind roll" is a private dice roll which where the result and details of the roll are only visible to Game Master users. The rolling player will not see the result of their own roll. This is similar to using a dice tower or other device at a physical tabletop where the roller may not get to see the outcome. To perform a blind roll, use <code>/blindroll</code>, <code>/broll</code>, or <code>/br</code> as the command prefix.</p>
	</dd>
	<dt>Self Roll</dt>
	<dd>
		<p>A "self roll" is a private dice roll which is only visible to the player who rolled it. To perform a self roll, use <code>/selfroll</code> or <code>/sr</code> as the command prefix.</p>
	</dd>
</dl>
<hr />

<h2 id="formula">Roll Formula Syntax</h2>
<p>Each dice term in a rolled share the same basic structure which chooses to roll a certain <strong>number</strong> of dice, each with a number of <strong>faces</strong>, and then apply optional <strong>modifiers</strong> to the rolled set.</p>
<pre><code class="language-plaintext">/roll {number}d{faces}{modifiers}</code></pre>
<p>Each rolled formula can contain multiple terms combined using arithmetic operations, and formulae can also include numeric terms. For example, the following formula will roll two 20-sided dice keeping the highest of the two, a separate 4-sided die, and add 4 to the total result.</p>
<pre><code class="language-plaintext">/roll 2d20kh + 1d4 + 4</code></pre>
<p>You may also include variable terms in your rolled expressions. These variable terms use the syntax <code>@{data.path}</code> which are evaluated using attributes from the data of your currently controlled Actor. The composition of this data depends on the game system you are using, but as an example, the following roll would add the "Strength modifier" in the D&amp;D5E system.</p>
<pre><code class="language-plaintext">/roll 1d6 + @abilities.str.mod</code></pre>
<hr />

<h2 id="modifiers">Available Roll Modifiers</h2>
<p>Each dice term can have "modifiers" applied to it. Each modifier is a set of additional characters which defines a type of operation to apply to the roll that it modifies. Modifiers are applied in order from left to right, so a roll with multiple modifiers will perform those in a predictable order where subsequent modifiers operate on the previously modified result. This section defines the roll modifiers which are currently supported:</p>

<h3 id="keep">Keep or Drop Results</h3>
<p>A desired number of high or low rolls may be kept or dropped using the following roll modifiers.</p>
<ul>
	<li>
		<p><code>kh{N}</code> - Keep the highest <code>N</code> dice out of the group that were rolled.</p>
	</li>
	<li>
		<p><code>kl{N}</code> - Keep the lowest <code>N</code> dice out of the group that were rolled.</p>
	</li>
	<li>
		<p><code>dh{N}</code> - Drop the highest <code>N</code> dice out of the group that were rolled.</p>
	</li>
	<li>
		<p><code>dl{N}</code> - Drop the lowest <code>N</code> dice out of the group that were rolled.</p>
	</li>
</ul>

<h3 id="reroll">Re-roll Certain Results</h3>
<p>Specific results may be re-rolled by specifying a re-roll target or range.</p>
<ul>
	<li>
		<p><code>r{y}</code> - Re-roll any dice where the result was <code>y</code>.</p>
	</li>
	<li>
		<p><code>r&gt;{y}</code> - Re-roll any dice where the result was greater than <code>y</code>.</p>
	</li>
	<li>
		<p><code>r&gt;={y}</code> - Re-roll any dice where the result was greater than or equal to <code>y</code>.</p>
	</li>
	<li>
		<p><code>r&lt;{y}</code> - Re-roll any dice where the result was less than <code>y</code>.</p>
	</li>
	<li>
		<p><code>r&lt;={y}</code> - Re-roll any dice where the result was less than or equal to <code>y</code>.</p>
	</li>
</ul>

<h3 id="explode">Explode Certain Results</h3>
<p>"Exploding" dice are supported where further dice of the same type are rolled until no more results of a certain value have been rolled.</p>
<ul>
	<li>
		<p><code>x{y}</code> - Roll an additional dice each time a die rolls <code>y</code>.</p>
	</li>
	<li>
		<p><code>x&gt;{y}</code> - Roll an additional dice if any dice rolls greater than <code>y</code>.</p>
	</li>
	<li>
		<p><code>x&gt;={y}</code> - Roll an additional dice if any dice rolls greater than or equal to <code>y</code>.</p>
	</li>
	<li>
		<p><code>x&lt;{y}</code> - Roll an additional dice if any dice rolls less than or equal to <code>y</code>.</p>
	</li>
	<li>
		<p><code>x&lt;={y}</code> - Roll an additional dice if any dice rolls less than or equal to <code>y</code>.</p>
	</li>
</ul>

<h3 id="successes">Count Successes</h3>
<p>You can roll a set of dice and count the number of times a certain result or range of results was achieved.</p>
<ul>
	<li>
		<p><code>cs={y}</code> - Count the number of dice which resulted exactly in <code>y</code>.</p>
	</li>
	<li>
		<p><code>cs&gt;{y}</code> - Count the number of dice which rolled greater than <code>y</code>.</p>
	</li>
	<li>
		<p><code>cs&gt;={y}</code> - Count the number of dice which rolled greater than or equal to <code>y</code>.</p>
	</li>
	<li>
		<p><code>cs&lt;{y}</code> - Count the number of dice which rolled less than or equal to <code>y</code>.</p>
	</li>
	<li>
		<p><code>cs&lt;={y}</code> - Count the number of dice which rolled less than or equal to <code>y</code>.</p>
	</li>
</ul>

<h3 id="margin">Margin of Success</h3>
<p>Roll a set of dice and compare the total against some target, keeping the difference as the result.</p>
<ul>
	<li>
		<p><code>ms={y}</code> - Subtract <code>y</code> from the rolled total and return the difference.</p>
	</li>
	<li>
		<p><code>ms&gt;{y}</code> - Same as above.</p>
	</li>
	<li>
		<p><code>ms&gt;={y}</code> - Same as above.</p>
	</li>
	<li>
		<p><code>ms&lt;{y}</code> - Subtract the rolled total from <cite>y</cite> and return the difference.</p>
	</li>
	<li>
		<p><code>ms&lt;={y}</code> - Same as above.</p>
	</li>
</ul>
<hr />

<h2 id="parantheticals">Parenthetical Expressions</h2>
<p>A parenthetical expression allows you to evaluate some inner component of a roll formula before evaluating the outer portion. This allows for options where the number, faces, or modifiers of a dice roll are themselves dynamic in some way. Parenthetical expressions are defined by enclosing the inner expression in parentheses. Using parenthetical expressions can allow you to roll a variable number of dice based on a data attribute or an inner dice roll, for example:</p>
<pre><code class="language-plaintext">/roll (@abilities.dex.mod)d6
/roll (2d4)d8
/roll (1d6)d(@attributes.proficiency)
</code></pre>
<p>Furthermore, parenthetical expressions can allow you to evaluate roll modifiers relative to a dynamically shifting target - for example to count the number of successes relative to some target attribute or opposed dice roll:</p> <pre><code class="language-plaintext">/roll 3d12cs&lt;=(@attributes.power)
/roll 3d12ms&gt;(4d6)
</code></pre>
<hr />

<h2 id="pools">Dice Pools</h2>
<p>A dice pool allows you to evaluate a set of dice roll expressions and combine or choose certain results from the pool as the final total. This allows you to keep, combine, or count results across multiple rolled formulae. Dice pools are defined using comma separated roll terms within brackets. Dice pools can be used to choose the better result between two or more alternative rolled formulae, for example:</p>
<pre><code class="language-plaintext">/roll {4d6 + 4, 3d8 + 3, 2d10 + 2}kh
/roll {1d20, 10}kh
</code></pre>
<p>Dice pools can also be used to count the number of inner results which exceeded some threshold, for example:</p> <pre><code class="language-plaintext">/roll {6d6, 5d8, 4d10, 3d12}cs&gt;15</code></pre>
<hr />

<h2 id="api">API References</h2>
<p>For module and system developers who want to go deeper with dice mechanics - there is a robust JavaScript API which can do even more with dice rolls. See the <a title="The Roll Class" href="/api/Roll.html" target="_blank" rel="nofollow noopener">Roll Class</a> API documentation for details.</p>